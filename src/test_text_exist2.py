from selenium import webdriver


def test_setup():
    global driver
    driver = webdriver.Chrome(executable_path="D:/Webdrivers/chromedriver.exe")
    driver.implicitly_wait(10)
    driver.maximize_window()


def test_text_appear():
    driver.get("https://the-internet.herokuapp.com/context_menu")
    str_to_look_for = "Right-click in the box below to see one called 'the-internet'"
    elems = driver.find_elements_by_xpath("//*[contains(text(),'Right-click in the box below to see one called')]")
    str_counter = 0;
    if len(elems) > 0:
        for elem in elems:
            if str_to_look_for in elem.text:
                str_counter += 1
    assert str_counter > 0


def test_teardown():
    driver.close()
    driver.quit()
    print("Test Completed")
