from selenium import webdriver


def test_setup():
    global driver
    driver = webdriver.Chrome(executable_path="D:/Webdrivers/chromedriver.exe")
    driver.implicitly_wait(10)
    driver.maximize_window()


def test_text_appear():
    driver.get("https://the-internet.herokuapp.com/context_menu")
    ali_elems = driver.find_elements_by_xpath("//*[contains(text(),'Alibaba')]")
    elem_count = len(ali_elems)
    assert elem_count > 0


def test_teardown():
    driver.close()
    driver.quit()
    print("Test Completed")
